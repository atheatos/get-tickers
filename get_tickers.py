"""Download a list of tickers from Yahoo Finance"""
import sys

import yfinance as yf


def main():
    """Open and parse the ticker file, download, then write to csv"""
    with open("tickers.txt", "r", encoding="utf-8") as ticker_fp:
        tickers = ticker_fp.read().strip().split("\n")

    results = yf.download(tickers, auto_adjust=False, period="max", interval="1d")
    results = results.droplevel(0, axis=1)
    results.to_csv("data.csv")


if __name__ == "__main__":
    sys.exit(main())
