# Installation

1. Configure your environment
```bash
pyenv install 3.10.2
pyenv virtualenv 3.10.2 get-tickers

pyenv activate get-tickers
python -m pip install -r requirements.txt
```

2. Add desired tickers to `tickers.txt`

3. Run the script
```bash
python get_tickers.py
```
